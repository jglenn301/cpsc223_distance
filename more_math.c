#include "more_math.h"

double to_radians(double a)
{
  return a * RADIANS_PER_DEGREE; // more_math.h has the #define in it
}
