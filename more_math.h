#ifndef __MORE_MATH_H__
#define __MORE_MATH_H__

// whether #defines like this go in .h or .c depend on whether you
// want users of your library to see them or not; by putting this one
// here I've chosen to allow users of my more_math library to use it
// by #include-ing "more_math.h"

#define RADIANS_PER_DEGREE (3.1415926535897932384 / 180.0)

/**
 * Returns the result of converting the given angle in degrees to the
 * equivalent in radians.
 *
 * @param a a double
 * @return the equivalent angle in radians
 */
double to_radians(double a);

#endif
