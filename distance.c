#include <stdio.h>

#include "geography.h"

/**
 * Computes the distance of a path given the latitudes and longitudes
 * of the points on the path.  The coordinates are read from standard input
 * and assumed to be in degrees.  The output is the number of points in the
 * path and the total distance using the great circle distance between adjacent
 * points.
 *
 * @version 0.1 2017-09-01
 */
int main()
{
  double total_distance = 0.0;

  double prev_lat, prev_lon;
  double curr_lat, curr_lon;

  int n = 0;
  while (scanf("%lf %lf", &curr_lat, &curr_lon) == 2)
    {
      n++;
      if (n >= 2)
	{
	  total_distance += distance(prev_lat, prev_lon, curr_lat, curr_lon);
	}
      prev_lat = curr_lat;
      prev_lon = curr_lon;
    }
  printf("%d points; total distance %lf miles\n", n, total_distance);
}



