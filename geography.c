#include <math.h>

#include "geography.h"
#include "more_math.h"

#define EARTH_RADIUS_MILES 3959
#define LATITUDE_NORTH_POLE_DEGREES 90.0

double distance(double lat1, double lon1, double lat2, double lon2)
{
  // compute colatitude and convert to radians
  double colat1 = colatitude(lat1);
  colat1 = to_radians(colat1);
  double colat2 = to_radians(colatitude(lat2)); // all in one line!

  // compute different in longitudes in radians
  double delta_lon = to_radians(lon1 - lon2);

  // use spherical law of cosines to compute angle
  double x = acos(cos(colat1) * cos(colat2) + sin(colat1) * sin(colat2) * cos(delta_lon));

  // convert radians to arc length by multiplying by radius
  return x * EARTH_RADIUS_MILES;
}

double colatitude(double lat)
{
  return LATITUDE_NORTH_POLE_DEGREES - lat;
}
