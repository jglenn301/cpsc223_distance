CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -g3

Distance: distance.o geography.o more_math.o
	${CC} ${CFLAGS} -o Distance distance.o geography.o more_math.o -lm

distance.o: distance.c geography.h more_math.h
	${CC} ${CFLAGS} -c distance.c

geography.o: geography.c
	${CC} ${CFLAGS} -c geography.c

more_math.o: more_math.c
	${CC} ${CFLAGS} -c more_math.c

clean:
	rm -r *.o Distance
