#ifndef __GEOGRAPHY_H__
#define __GEOGRAPHY_H__

// the #ifndef...#define...#endif is a guard against double inclusion

/**
 * Computes the distance of the great circle route between two points.
 * The latitudes and longitudes should be in degrees and the distance
 * is computes assuming that the Earth is spherical.
 *
 * @param lat1 a double between -90 and +90
 * @param lon1 a double between -180 and 180
 * @param lat2 a double between -90 and +90
 * @param lon2 a double between -180 and 180
 * @return the distance between (lat1, lon1) and (lat2, lon2)
 */
double distance(double lat1, double lon1, double lat2, double lon2);


/**
 * Returns the colatitude of the given latitude in degrees.  The given latitude
 * should be in degrees.
 *
 * @param lat a double between -90 and +90
 * @return the colatitude of lat, in degrees
 */
double colatitude(double); // formal parameter names optional in declaration

#endif
